import java.awt.*;

public class Cible extends Entite
{
	// immobile par défaut
	private boolean mobile;

	public Cible()
	{
		this.pos = new Vecteur2();
		this.initialPos = new Vecteur2();
		this.mobile = false;
		this.color = Color.RED;
	}

	public Cible(Vecteur2 pos)
	{
		this.pos = new Vecteur2();
		this.initialPos = new Vecteur2(pos.x, pos.y);
		this.mobile = false;
		this.color = Color.RED;
	}

	public Cible(double x, double y)
	{
		this.pos = new Vecteur2(x, y);
		this.initialPos = new Vecteur2(x, y);
		this.mobile = false;
		this.color = Color.RED;
	}

	public boolean getMobile()
	{
		return mobile;
	}

	public void setMobile(boolean mobile)
	{
		this.mobile = mobile;
	}

	public void bouge()
	{
		// si la cible est mobile, la faire bouger
		if (mobile)
		{
		}
	}

	public void animate()
	{

	}

	public String toString()
	{
		return super.toString() + "\n" + "Cible";
	}
}
