import java.awt.*;

public class HUD extends Entite
{
	private int points;

	public HUD()
	{
		this.pos = new Vecteur2();
		this.initialPos = new Vecteur2();
		this.points = 0;
		this.color = Color.BLACK;
	}

	public HUD(Vecteur2 pos)
	{
		this.pos = new Vecteur2(pos.x, pos.y);
		this.initialPos = new Vecteur2(pos.x, pos.y);
		this.points = 0;
		this.color = Color.BLACK;
	}

	public HUD(double x, double y)
	{
		this.pos = new Vecteur2(x, y);
		this.initialPos = new Vecteur2(x, y);
		this.points = 0;
		this.color = Color.BLACK;
	}

	public void bouge()
	{
		//le hud est immobile
	}

	public void animate()
	{
		// pas d'animation
	}

	public int getPoints()
	{
		return points;
	}

	public void setPoints(int points)
	{
		this.points = points;
	}
}
