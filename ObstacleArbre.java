import java.util.*;
import java.awt.*;

public class ObstacleArbre extends Obstacle
{
	private ArrayList<String> sprite;
	private int frame;

	public ObstacleArbre()
	{
		super();
		frame = 0;
		sprite = new ArrayList<String>();
		setupSprite();
		color = Color.GREEN;
	}

	public ObstacleArbre(Vecteur2 pos)
	{
		super(pos);
		frame = 0;
		sprite = new ArrayList<String>();
		setupSprite();
		color = Color.GREEN;
	}

	public ObstacleArbre(double x, double y)
	{
		super(x, y);
		frame = 0;
		sprite = new ArrayList<String>();
		setupSprite();
		color = Color.GREEN;
	}

	public void bouge()
	{
		animate();
		// un arbre ne bouge pas
	}

	public void animate()
	{
		if (frame++ % 10 == 0)
		{
			sprite.add("   ##   ");
			registerSprite();
		}
	}

	public final void resetSprite()
	{
		setupSprite();
	}

	private final void setupSprite()
	{
		sprite.clear();

		sprite.add("  ####  ");
		sprite.add(" ###### ");
		sprite.add("########");
		sprite.add(" ###### ");
		sprite.add("   ##   ");

		registerSprite();
	}

	private final void registerSprite()
	{
		String[] sprite_array = new String[sprite.size()];
		sprite.toArray(sprite_array);

		String[][] sprite_array_wrapper = new String[1][];
		sprite_array_wrapper[0] = sprite_array;

		super.setFromSprite(sprite_array_wrapper);
	}
}
