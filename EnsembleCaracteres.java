import java.util.ArrayList;
import java.awt.*;

public class EnsembleCaracteres
{
	private ArrayList<CaracterePositionne> caracteres;

	EnsembleCaracteres()
	{
		caracteres = new ArrayList<CaracterePositionne>();
	}

	public void ajouteCar(double x, double y, char c, Color col)
	{
		caracteres.add(new CaracterePositionne(x, y, c, col));
	}

	public CaracterePositionne getCar(int i)
	{
		return caracteres.get(i);
	}

	public void vider()
	{
		caracteres.clear();
	}

	public boolean estVide()
	{
		return caracteres.size() == 0;
	}

	public ArrayList<CaracterePositionne> getCaracteres()
	{
		return caracteres;
	}

	public EnsembleCaracteres union(EnsembleCaracteres ens)
	{
		EnsembleCaracteres[] t = { this, ens };

		return EnsembleCaracteres.union(t);
	}

	// static version of union
	public static EnsembleCaracteres union(EnsembleCaracteres[] tens)
	{
		EnsembleCaracteres u = new EnsembleCaracteres();

		for (EnsembleCaracteres e : tens)
		{
			for (CaracterePositionne c : e.getCaracteres())
			{
				u.ajouteCar(c.getPosition().x, c.getPosition().y, c.getChar(), c.getColor());
			}
		}

		return u;
	}
}
