public class Executable
{
	public static void main(String[] args)
	{
		String[][] spriteBombe = {
			{
				"/ \\",
				"\\#/"
			},
			{
				"/#\\",
				"\\ /"
			}
		};

		String[][] spriteCible = {
			{
				"c--o",
				" ( ^)"
			},
			{
				"c--o",
				" (^ )"
			}
		};

		String[][] spriteOiseau = {
			{
				"<O===/",
				"  \\/  "
			},
			{
				"   /\\ ",
				"<O===/"
			},
			{
				"\\===O>",
				"  \\/  "
			},
			{
				"  /\\  ",
				"\\===O>"
			}
		};

		String[][] spriteHUD = {
			{
				"╔══════════════════════════════════════════════════════════════════════════════╗",
				"║       (>¨)>  *GORILLA GAME*  <(¨<)        Points : 0     Vent : 0            ║",
				"╚══════════════════════════════════════════════════════════════════════════════╝"
			}
		};

		Fenetre f = new Fenetre();

		Projectile p = new Projectile(5, 5);
		p.setFromSprite(spriteBombe);

		Cible c = new Cible(60, 5);
		c.setFromSprite(spriteCible);

		//pas de sprite pour l'arbre, il est généré à l'execution
		ObstacleArbre a = new ObstacleArbre(30, 0);

		ObstacleOiseau o = new ObstacleOiseau(65, 20);
		o.setFromSprite(spriteOiseau);

		HUD hud = new HUD(0, 35);
		hud.setFromSprite(spriteHUD);


		GorillaGame jeu  = new GorillaGame(f);

		jeu.registerProjectile(p);
		jeu.registerHUD(hud);

		jeu.registerEntite(c);
		jeu.registerEntite(a);
		jeu.registerEntite(o);

		jeu.setTracingMode(true);

		do
		{
			jeu.getProjectile().setVent(Utils.randDouble(-9.0, 9.0));
			jeu.debutTour();

			while (!jeu.finTour())
			{
				jeu.evolue();
				f.pause(110);
				jeu.affiche();
			}
		} while (!jeu.finJeu());

		System.out.println("A bientôt !");
	}
}
