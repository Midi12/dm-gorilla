import java.lang.Math;
import java.util.Random;

public class Utils
{
	public static double DegresVersRadians(double d)
	{
		return (d * Math.PI) / 180.0;
	}

	public static int randInt(int min, int max)
	{
		Random rand = new Random();
		return rand.nextInt((max - min) + 1) + min;
	}

	public static double randDouble(double min, double max)
	{
		Random rand = new Random();
		return min + (max - min) * rand.nextDouble();
	}
}
