import java.awt.*;

public class ObstacleOiseau extends Obstacle
{
	private int dir;
	private int frame;

	public ObstacleOiseau()
	{
		super();
		dir = 1;
		frame = 0;
		color = Color.BLUE;
	}

	public ObstacleOiseau(Vecteur2 pos)
	{
		super(pos);
		dir = 1;
		frame = 0;
		color = Color.BLUE;
	}

	public ObstacleOiseau(double x, double y)
	{
		super(x, y);
		dir = 1;
		frame = 0;
		color = Color.BLUE;
	}

	public void bouge()
	{
		animate();

		if (frame++ % 20 == 0)
			dir *= -1;

		pos.x += dir;
	}

	public void animate()
	{
		if (dir == -1)
		{
			if (idxAnim + 1 >= getNumAnim() - 2)
				idxAnim = 0;
			else
				idxAnim = 1;
		}
		else
		{
			if (idxAnim + 1 >= getNumAnim())
				idxAnim = 2;
			else
				idxAnim = 3;
		}
	}
}
