import java.awt.*;

public class CaracterePositionne
{
	private Vecteur2 pos;
	private char c;
	private Color color;

	public CaracterePositionne(double x, double y, char c, Color col)
	{
		this.pos = new Vecteur2(x, y);
		this.c = c;
		this.color = col;
	}

	public char getChar()
	{
		return c;
	}

	public void setChar(char c)
	{
		this.c = c;
	}

	public Vecteur2 getPosition()
	{
		return pos;
	}

	public Color getColor()
	{
		return color;
	}
}
