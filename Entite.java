import java.util.*;
import java.awt.*;

public abstract class Entite
{
	protected Vecteur2 pos;
	protected Vecteur2 initialPos;
	protected int idxAnim;
	private int numAnim;
	private EnsembleCaracteres[] ensemble;
	protected Color color;

	public final EnsembleCaracteres getEnsembleCaracteres()
	{
		return ensemble[idxAnim >= ensemble.length ? ensemble.length - 1 : idxAnim];
	}

	public final Vecteur2 getInitialPos()
	{
		return initialPos;
	}

	public final Vecteur2 getPosition()
	{
		return pos;
	}

	public final void setPosition(Vecteur2 pos)
	{
		this.pos = new Vecteur2(pos.x, pos.y);
	}

	public final void setColor(Color col)
	{
		this.color = col;
	}

	abstract public void bouge(); // a definir dans les classes filles !
	abstract public void animate(); // a definir dans les classes filles !

	public final void setFromSprite(String[][] sprites)
	{
		this.ensemble = new EnsembleCaracteres[sprites.length];
		for (int k = 0; k < ensemble.length; k++)
			ensemble[k] = new EnsembleCaracteres();

		this.idxAnim = 0;

		this.numAnim = 0;

		for (String[] sprite : sprites)
		{
			int deltay = sprite.length;
			for (String s : sprite)
			{
				int deltax = 0;
				for (char c : s.toCharArray())
				{
					ensemble[numAnim].ajouteCar(deltax, deltay, c, this.color);
					deltax++;
				}
				deltay--;
			}
			numAnim++;
		}
	}

	public final int getNumAnim()
	{
		return numAnim;
	}

	public String toString()
	{
		return "Entite : " + this;
	}
}
