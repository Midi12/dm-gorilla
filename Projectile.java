import java.lang.Math;
import java.awt.*;

public class Projectile extends Entite
{
	private double force;
	private int angle;
	private double vent;
	private int frameCounter;

	public Projectile()
	{
		this.pos = new Vecteur2();
		this.initialPos = new Vecteur2();
		this.color = Color.BLACK;
		this.vent = 0.0;
		this.frameCounter = 0;
	}

	public Projectile(Vecteur2 pos)
	{
		this.pos = new Vecteur2(pos.x, pos.y);
		this.initialPos = new Vecteur2(pos.x, pos.y);
		this.angle = 0;
		this.force = 0.0;
		this.color = Color.BLACK;
		this.vent = 0.0;
		this.frameCounter = 0;
	}

	public Projectile(double x, double y)
	{
		this.pos = new Vecteur2(x, y);
		this.initialPos = new Vecteur2(x, y);
		this.angle = 0;
		this.force = 0.0;
		this.color = Color.BLACK;
		this.vent = 0.0;
		this.frameCounter = 0;
	}

	public void resetFrameCounter()
	{
		this.frameCounter = 0;
	}

	public void setAngle(int angle)
	{
		this.angle = angle;
	}

	public int getAngle()
	{
		return this.angle;
	}

	public void setForce(double force)
	{
		this.force = force;
	}

	public double getForce()
	{
		return this.force;
	}

	public void setVent(double vent)
	{
		this.vent = vent;
	}

	public double getVent()
	{
		return this.vent;
	}

	public void bouge()
	{
		animate();

		/*double deltaT = 1 / (Math.sqrt(Math.pow(vitesse.x, 2) + Math.pow(vitesse.y, 2)));
		pos.x += vitesse.x * deltaT;
		pos.y += vitesse.y * deltaT;
		vitesse.y += accel;*/

		final double g = 9.80665;

		double angleRad = Utils.DegresVersRadians(this.angle >= 88 ? 87 : this.angle); // ternaire pour rendre le jeu plus jouable
		double forceAug = (this.force * 5) + (this.vent * 20); // valeur arbitraire

		if (forceAug <= 0)
			forceAug = 6.0;

		System.out.println("angleRad = " + angleRad);
		System.out.println("forceAug = " + forceAug);

		// formule tir depuis l'origine du repère
		//pos.x += 0.5; // offset arbitraire
		//pos.y = (-g / (2 * this.force * Math.cos(angleRad)) * Math.pow(pos.x, 2)) + Math.tan(angleRad) * pos.x;

		// formule tir depuis n'importe quelle position
		pos.x += 0.5; // offset arbitraire
		pos.y = ((-g / (2 * forceAug * Math.cos(angleRad))) * Math.pow(pos.x, 2))
				+ (((g * initialPos.x) / (forceAug * Math.cos(angleRad)) + Math.tan(angleRad)) * (pos.x))
				+ (initialPos.x * (((-initialPos.x * g) / (2 * forceAug * Math.cos(angleRad))) - Math.tan(angleRad)))
				+ initialPos.y;

		System.out.println("proj pos = " + this.pos);
	}

	public void animate()
	{
		if (idxAnim + 1 >= getNumAnim())
			idxAnim = 0;
		else
			idxAnim++;
	}

	public String toString()
	{
		return super.toString() + "\n" + "Projectile";
	}

	public boolean touche(Entite e)
	{
		boolean hit = false;

		//real func
		CaracterePositionne c;
		for(int i = 0; i < e.getEnsembleCaracteres().getCaracteres().size() && !hit; i++)
		{
			c = e.getEnsembleCaracteres().getCaracteres().get(i);
			if (c.getChar() != ' ')
			{
				if (Math.abs(e.getPosition().x + c.getPosition().x - this.getPosition().x) <= 1.5)
					if (Math.abs(e.getPosition().y + c.getPosition().y - this.getPosition().y) <= 1.5)
						hit = true;
			}
		}
		return hit;
	}
}
