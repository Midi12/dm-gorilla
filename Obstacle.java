import java.awt.*;

public class Obstacle extends Entite
{
	public Obstacle()
	{
		this.pos = new Vecteur2();
		this.initialPos = new Vecteur2();
		this.color = Color.BLACK;
	}

	public Obstacle(Vecteur2 pos)
	{
		this.pos = new Vecteur2();
		this.initialPos = new Vecteur2(pos.x, pos.y);
		this.color = Color.BLACK;
	}

	public Obstacle(double x, double y)
	{
		this.pos = new Vecteur2(x, y);
		this.initialPos = new Vecteur2(x, y);
		this.color = Color.BLACK;
	}

	public void bouge()
	{
		// un obstacle de base ne bouge pas
	}

	public void animate()
	{
		if (idxAnim + 1 >= getNumAnim())
			idxAnim = 0;
		else
			idxAnim++;
	}

	public String toString()
	{
		return super.toString() + "\n" + "Obstacle";
	}
}
