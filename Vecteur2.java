public class Vecteur2
{
	// attributs public et pas de getters/setters car represente mieux un vecteur (x, y) de cette façon
	public double x, y;

	public Vecteur2()
	{
		this.x = this.y = 0.0;
	}

	public Vecteur2(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public String toString()
	{
		return "Vecteur2(" + this.x + ", " + this.y + ")";
	}
}
