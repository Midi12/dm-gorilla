import java.util.*;
import java.lang.Math;
import java.awt.*;

public class GorillaGame
{
	private ArrayList<Entite> listeEntite;
	private Projectile projectile;
	private HUD hud;
	private boolean tracing, debug;
	private EnsembleCaracteres trace;
	private Fenetre fen;

	public GorillaGame(Fenetre f)
	{
		listeEntite = new ArrayList<Entite>();
		trace = new EnsembleCaracteres();
		tracing = false;
		debug = false;
		this.fen = f;
	}

	public void setTracingMode(boolean enable)
	{
		tracing = enable;
	}

	public boolean getTracingMode()
	{
		return tracing;
	}

	public void registerEntite(Entite e)
	{
		listeEntite.add(e);
	}

	public void registerProjectile(Projectile p)
	{
		this.projectile = p;
	}

	public Projectile getProjectile()
	{
		return this.projectile;
	}

	public void registerHUD(HUD hud)
	{
		this.hud = hud;
	}

	public HUD getHUD()
	{
		return this.hud;
	}

	public void affiche()
	{
		afficheVent();

		EnsembleCaracteres[] entitees = new EnsembleCaracteres[listeEntite.size() + (tracing ? 3 : 2)];

		int i = 0;
		for (i = 0; i < entitees.length - (tracing ? 3 : 2); i++)
		{
			entitees[i] = new EnsembleCaracteres();
			for (CaracterePositionne c : listeEntite.get(i).getEnsembleCaracteres().getCaracteres())
			{
				entitees[i].ajouteCar(listeEntite.get(i).getPosition().x + c.getPosition().x,
										listeEntite.get(i).getPosition().y + c.getPosition().y,
										c.getChar(), c.getColor());
			}
		}

		entitees[i] = new EnsembleCaracteres();
		for (CaracterePositionne c : projectile.getEnsembleCaracteres().getCaracteres())
			entitees[i].ajouteCar(projectile.getPosition().x + c.getPosition().x,
									projectile.getPosition().y + c.getPosition().y,
									c.getChar(), c.getColor());

		entitees[i + 1] = new EnsembleCaracteres();
		for (CaracterePositionne c : hud.getEnsembleCaracteres().getCaracteres())
			entitees[i + 1].ajouteCar(hud.getPosition().x + c.getPosition().x,
									hud.getPosition().y + c.getPosition().y,
									c.getChar(), c.getColor());

		if (tracing)
		{
			trace.ajouteCar(projectile.getPosition().x, projectile.getPosition().y + 2, '.', Color.BLACK);
			entitees[i + 2] = trace;
		}

		this.fen.afficherCaracteres(EnsembleCaracteres.union(entitees));
	}

	public void evolue()
	{
		for (Entite e : listeEntite)
			e.bouge();

		projectile.bouge();
	}

	public boolean finJeu()
	{
		String str = "";
		ArrayList<String> correct = new ArrayList<String>();
		correct.add("o");
		correct.add("n");

		do
		{
			Scanner sc = new Scanner(System.in);
			System.out.println("Continuer ? (O/N)");
			str = sc.nextLine().toLowerCase();
			System.out.println(str);
		} while (!correct.contains(str));

		return !str.equals("o");
	}

	public boolean finTour()
	{
		boolean fin = /*projectile.getPosition().x < -1 ||*/ projectile.getPosition().x >= this.fen.getNbColonnes() || projectile.getPosition().y < -1 /*|| projectile.getPosition().y >= this.fen.getNbLignes()*/;

		if (!fin)
		{
			for (int i = 0; i < listeEntite.size() && !fin; i++)
				if (projectile.touche(listeEntite.get(i)))
				{
					fin = true;

					if (listeEntite.get(i) instanceof Cible)
					{
						hud.setPoints(hud.getPoints() + 1);
						afficherPoints();
						this.affiche();
					}
				}
		}

		return fin;
	}

	public void debutTour()
	{
		projectile.setPosition(projectile.getInitialPos());
		//projectile.setPosition(new Vecteur2(Utils.randDouble(1.0, 7.0), Utils.randDouble(1.0, 5.0)));

		for (Entite e : listeEntite)
		{
			if (e instanceof ObstacleArbre)
			{
				((ObstacleArbre)e).resetSprite();
				((ObstacleArbre)e).setPosition(new Vecteur2(Utils.randDouble(10.0, 25.0), 0.0));
			}
			else
				e.setPosition(new Vecteur2(Utils.randDouble(15.0, 65.0), Utils.randDouble(15.0, 30.0)));
		}

		trace.vider();

		this.affiche();

		int angle = -1;
		double force = -1;

		try
		{
			do
			{
				Scanner sc = new Scanner(System.in);
				System.out.println("Entrez l'angle : ");
				String str = sc.nextLine();
				angle = Integer.parseInt(str);
			} while (angle < 0 || angle > 90);

			do
			{
				Scanner sc = new Scanner(System.in);
				System.out.println("Entrez la force : ");
				String str = sc.nextLine();
				force = Double.parseDouble(str);
			} while (force < 0 || force > 100);
		}
		catch(Exception e)
		{
			System.out.println("Données malformées, données de base utilisées (angle 45, force 50)");
			angle = 45;
			force = 50;
		}

		projectile.setAngle(angle);
		projectile.setForce(force);
	}

	public boolean getDebugMode()
	{
		return debug;
	}

	public void setDebugMode(boolean enable)
	{
		debug = enable;
	}

	private void afficheVent()
	{
		// la chaine "Vent : " est à un offset fixe ( donc la position où écrire est à l'offset fixe 146) dans le tableau
		String vt = String.valueOf((int)Math.abs(projectile.getVent()));
		for (int i = 146; i < 146 + vt.length(); i++)
			hud.getEnsembleCaracteres().getCar(i).setChar(vt.charAt(i - 146));

		if (projectile.getVent() < 0)
			hud.getEnsembleCaracteres().getCar(150).setChar('<');
		else
			hud.getEnsembleCaracteres().getCar(150).setChar('>');
	}

	private void afficherPoints()
	{
		// la chaine "Points : " est à un offset fixe ( donc la position où écrire est à l'offset fixe 133) dans le tableau
		String pts = String.valueOf(hud.getPoints());
		for (int i = 133; i < 133 + pts.length(); i++)
			hud.getEnsembleCaracteres().getCar(i).setChar(pts.charAt(i - 133));
	}
}
